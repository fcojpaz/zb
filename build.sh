#!/bin/env sh
pushd `dirname $0` > /dev/null
SCRIPT_PATH=`pwd -P`
popd > /dev/null

echo "Cleaning build..."
rm -rf src dep test *.a
if [[ $1 = "clean" ]]; then
    exit
fi

CC="clang"
CFLAGS="-g -Wall"
FILES="$SCRIPT_PATH/src/*.c"

echo "Building libzb..."
mkdir -p src
for file in $FILES
do
    FILE_NAME_WITH_EXT="${file#$SCRIPT_PATH/}"
    FILE_NAME="${FILE_NAME_WITH_EXT%.c}"
    $CC $CFLAGS -c "$file" -o "$FILE_NAME.o"
done
ar rcs libzb.a src/*.o

echo "Building libmunit..."
mkdir -p dep/munit
MUNIT_OBJ=dep/munit/munit.o
$CC $CFLAGS -c "$SCRIPT_PATH"/dep/munit/munit.c -o "$MUNIT_OBJ"
ar rcs libmunit.a "$MUNIT_OBJ"

echo "Building test..."
$CC $CFLAGS -o test "$SCRIPT_PATH"/test/test.c -L. -static -lzb -lmunit
