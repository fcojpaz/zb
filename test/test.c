// (C) 2017 Francisco Javier Paz
//
// This software is provided 'as-is', without any express or implied
// warranty.  In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
#define MUNIT_ENABLE_ASSERT_ALIASES
#include "../dep/munit/munit.h"
#include "../src/align_inl.h"
#include "../src/alloc_inl.h"
#include "../src/common_inl.h"
#include "../src/memory.h"
#include "../src/pointer_inl.h"

// pointer suite
static MunitResult pointer_align(const MunitParameter params[], void *fixture)
{
    ZB_UNUSED(params);
    ZB_UNUSED(fixture);
    // Returns next address multiple of alignment
    const uint16_t alignment = 64;
    void *expected = (void *)(uintptr_t)alignment;
    void *ptr = (void *)10;
    void *aligned = zb_pointer_align(ptr, alignment);
    assert_ptr_equal(aligned, expected);
    // If address is multiple of alignment returns address
    ptr = expected;
    aligned = zb_pointer_align(ptr, alignment);
    assert_ptr_equal(aligned, expected);
    return MUNIT_OK;
}

// std_alloc suite
static void *std_alloc_setup(const MunitParameter params[], void *user_data)
{
    ZB_UNUSED(params);
    ZB_UNUSED(user_data);
    struct zb_alloc *alloc = malloc(sizeof(*alloc));
    zb_std_alloc_init(alloc);
    return alloc;
}

static void std_alloc_teardown(void *fixture) { free(fixture); }

static MunitResult
std_alloc_returns_not_null_if_mem_available(const MunitParameter params[],
                                            void *fixture)
{
    ZB_UNUSED(params);
    struct zb_alloc *alloc = fixture;
    const size_t ALLOC_SIZE = 128;
    void *ptr = zb_malloc(alloc, ALLOC_SIZE, 0);
    assert_not_null(ptr);
    ptr = zb_realloc(alloc, ptr, ALLOC_SIZE, 0);
    assert_not_null(ptr);
    free(ptr);
    ptr = zb_calloc(alloc, ALLOC_SIZE, 0);
    assert_not_null(ptr);
    free(ptr);
    return MUNIT_OK;
}

static MunitResult std_alloc_aligns_memory(const MunitParameter params[],
                                           void *fixture)
{
    ZB_UNUSED(params);
    struct zb_alloc *alloc = fixture;
    const uint16_t alignment = 64;
    const size_t ALLOC_SIZE = 100;
    void *ptr = zb_malloc(alloc, ALLOC_SIZE, alignment);
    uint16_t modulus = (size_t)ptr % alignment;
    assert_uint16(modulus, ==, 0);
    free(ptr);
    ptr = zb_realloc(alloc, NULL, ALLOC_SIZE, alignment);
    modulus = (size_t)ptr % alignment;
    assert_uint16(modulus, ==, 0);
    free(ptr);
    ptr = zb_calloc(alloc, ALLOC_SIZE, alignment);
    modulus = (size_t)ptr % alignment;
    assert_uint16(modulus, ==, 0);
    free(ptr);
    return MUNIT_OK;
}

static MunitResult std_alloc_calloc_zeroes_mem(const MunitParameter params[],
                                               void *fixture)
{
    ZB_UNUSED(params);
    struct zb_alloc *alloc = fixture;
    const size_t ALLOC_SIZE = 100;
    char zeroes[100] = {0};
    void *ptr = zb_calloc(alloc, ALLOC_SIZE, 0); // calloc
    assert_true(!memcmp(ptr, zeroes, ALLOC_SIZE));
    free(ptr);
    ptr = zb_calloc(alloc, ALLOC_SIZE, 256); // aligned_alloc + memset
    assert_true(!memcmp(ptr, zeroes, ALLOC_SIZE));
    free(ptr);
    return MUNIT_OK;
}

int main(int argc, char *argv[])
{
    int ret = EXIT_SUCCESS;
    {
        // pointer
        MunitTest tests[] = {
            {"/align,", pointer_align, NULL, NULL, MUNIT_TEST_OPTION_NONE,
             NULL},
            {NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL}};
        const MunitSuite suite = {"/pointer", tests, NULL, 1,
                                  MUNIT_SUITE_OPTION_NONE};
        ret &= munit_suite_main(&suite, NULL, argc, argv);
    }
    {
        // std_alloc
        MunitTest tests[] = {
            {"/returns_not_null_if_mem_available,",
             std_alloc_returns_not_null_if_mem_available, std_alloc_setup,
             std_alloc_teardown, MUNIT_TEST_OPTION_NONE, NULL},
            {"/aligns_memory,", std_alloc_aligns_memory, std_alloc_setup,
             std_alloc_teardown, MUNIT_TEST_OPTION_NONE, NULL},
            {"/calloc_zeroes_mem,", std_alloc_calloc_zeroes_mem,
             std_alloc_setup, std_alloc_teardown, MUNIT_TEST_OPTION_NONE, NULL},
            {NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL}};
        const MunitSuite suite = {"/std_alloc", tests, NULL, 1,
                                  MUNIT_SUITE_OPTION_NONE};
        ret &= munit_suite_main(&suite, NULL, argc, argv);
    }

    return ret;
}
