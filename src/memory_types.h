// (C) 2017 Francisco Javier Paz
//
// This software is provided 'as-is', without any express or implied
// warranty.  In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
#pragma once
#ifdef __cplusplus
extern "C" {
#endif
///////////////////////////////////////////////////////////////////////////////
#include <stddef.h>
#include <stdint.h>

struct zb_alloc {
    void *(*malloc)(struct zb_alloc *alloc, size_t size, uint16_t alignment);
    void *(*calloc)(struct zb_alloc *alloc, size_t size, uint16_t alignment);
    void *(*realloc)(struct zb_alloc *alloc, void *ptr, size_t size,
                     uint16_t alignment);
    void (*free)(struct zb_alloc *alloc, void *ptr);
};
///////////////////////////////////////////////////////////////////////////////
#ifdef __cplusplus
}
#endif
