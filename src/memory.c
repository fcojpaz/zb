// (C) 2017 Francisco Javier Paz
//
// This software is provided 'as-is', without any express or implied
// warranty.  In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
#include "memory.h"
#include "align_inl.h"
#include "common_inl.h"
#include <stdalign.h>
#include <stdlib.h>
#include <string.h>

void zb_std_alloc_init(struct zb_alloc *alloc)
{
    alloc->malloc = &zb_std_malloc;
    alloc->calloc = &zb_std_calloc;
    alloc->realloc = &zb_std_realloc;
    alloc->free = &zb_std_free;
}

void *zb_std_malloc(struct zb_alloc *alloc, size_t size, uint16_t alignment)
{
    ZB_UNUSED(alloc);
    if (0 == alignment)
        alignment = sizeof(max_align_t);
    size = zb_size_align(size, alignment);
    return aligned_alloc(alignment, size);
}

void *zb_std_calloc(struct zb_alloc *alloc, size_t size, uint16_t alignment)
{
    ZB_UNUSED(alloc);
    if (0 == alignment)
        alignment = sizeof(max_align_t);
    size = zb_size_align(size, alignment);
    if (alignment <= sizeof(max_align_t)) // fast path
        return calloc(1, size);           // aligned to sizeof(max_align_t)

    // Slow path. No portable way to use calloc with
    // alignment > sizeof(max_align_t).
    void *ptr = aligned_alloc(alignment, size);
    if (ptr)
        memset(ptr, 0, size);
    return ptr;
}

void *zb_std_realloc(struct zb_alloc *alloc, void *ptr, size_t size,
                     uint16_t alignment)
{
    ZB_UNUSED(alloc);
    if ((NULL != ptr) && (0 != size)) {
        if (0 == alignment)
            alignment = sizeof(max_align_t);
        size = zb_size_align(size, alignment);
        return realloc(ptr, size);
    } else if ((NULL == ptr) && (0 != size)) {
        if (0 == alignment)
            alignment = sizeof(max_align_t);
        size = zb_size_align(size, alignment);
        return aligned_alloc(alignment, size);
    } else {
        free(ptr);
        ptr = NULL;
        return ptr;
    }
}

void zb_std_free(struct zb_alloc *alloc, void *ptr)
{
    ZB_UNUSED(alloc);
    free(ptr);
    ptr = NULL;
}
