// (C) 2017 Francisco Javier Paz
//
// This software is provided 'as-is', without any express or implied
// warranty.  In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
#pragma once
#ifdef __cplusplus
extern "C" {
#endif
///////////////////////////////////////////////////////////////////////////////
#include "common_inl.h"
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

ZB_INLINE bool zb_power_of_2(uint16_t x)
{
    return ((x != 0) && ((x & (~x + 1)) == x));
}

ZB_INLINE size_t zb_size_align(size_t size, uint16_t alignment)
{
    assert(zb_power_of_2(alignment));
    return (size + alignment - 1) & ~(alignment - 1);
}

ZB_INLINE void *zb_pointer_align(void *ptr, uint16_t alignment)
{
    assert(zb_power_of_2(alignment));
    return (void *)((uintptr_t)(((uint8_t *)ptr) + (alignment - 1)) &
                    ~(alignment - 1));
}
///////////////////////////////////////////////////////////////////////////////
#ifdef __cplusplus
}
#endif
