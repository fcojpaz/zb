µnit
====
µnit is a small and portable unit testing framework for C which includes pretty much everything you might expect from a C testing framework, plus a few pleasant surprises, wrapped in a nice API.

µnit is licensed under the terms of [MIT License](https://opensource.org/licenses/MIT).

Upstream version [here](https://github.com/nemequ/munit).
